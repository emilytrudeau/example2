module.exports = {
  someSidebar: {
    'Getting Started': ['GettingStarted', 'QuickStart'],
    'Work with Data': ['FindingData', 'CreateObjects', 'SettingObjectMetadata'],
    'Data Model':['hierarchy', 'permissions', 'custom_roles', 'containers'],
    'Flywheel Views':['views'],
    'SDK Gears':['sdk_gears'],
    'Tutorials':['tutorial_AddSubjectsToCollectionExcludingThoseInACSV','tutorial_BatchRunFlywheelGearsWithSDK','tutorial_ DeleteEmptyContainers','tutorial_ RunLocalAnalysisAndUploadBackToFlywheel','tutorial_editAcquisitionTimestamp','tutorial_FindOutdatedGearRulesandUpdate','tutorial_GettingandReloadingContainers','tutorial_JobMonitoringAdminVersion','tutorial_JobMonitoringUserandDeveloperVersion','tutorial_UpdateMoCoAcquisitionLabel','upload-data-to-a-new-project']
  },
};
