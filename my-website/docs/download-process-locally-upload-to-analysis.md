---
id: tutorial_ RunLocalAnalysisAndUploadBackToFlywheel  
title: Run local analysis and upload back to Flywheel  
sidebar_label: Run local analysis and upload back to Flywheel  
slug: /tutorial-Run-local-analysis-and-upload back-to-Flywheel  
---
**Title**: Run local analysis and upload back to Flywheel  
**Date**:  15-Apr-2020  
**Description**:  
Find and download input file from flywheel, process locally, create an analysis container and upload local process outputs to it.

Topics that will be covered:
* Find file in Flywheel Acquisition container
* Download file and run analysis locally
* Create Analysis container
* Local outputs are uploaded to the Flywheel Analysis container.

# Install and import dependencies


```python
# Install specific packages required for this notebook
!pip install flywheel-sdk nipype
```


```python
# Import packages
from getpass import getpass
import logging
import os
from pathlib import Path

import flywheel
import nipype
from nipype.interfaces.image import Reorient
from permission import check_user_permission

```


```python
# Instantiate a logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
log = logging.getLogger('root')
```

# Flywheel API Key and Client

Get a API_KEY. More on this in the Flywheel SDK doc [here](https://flywheel-io.gitlab.io/product/backend/sdk/branches/master/python/getting_started.html#api-key).


```python
API_KEY = getpass('Enter API_KEY here: ')
```

Instantiate the Flywheel API client


```python
fw = flywheel.Client(API_KEY if 'API_KEY' in locals() else os.environ.get('FW_KEY'))
```

Show Flywheel logging information


```python
log.info('You are now logged in as %s to %s', fw.get_current_user()['email'], fw.get_config()['site']['api_url'])
```

# Requirements

Before starting off, we want to check your permission on the Flywheel Instance in order to proceed in this notebook. 


```python
min_reqs = {
    "site": "user",
    "group": "ro",
    "project": ['analyses_create_sdk','files_view_contents','files_download','files_create_upload',]
    
}
```

<div class="alert alert-block alert-info" style="color:black"><b>Tip:</b> Group ID and Project Label can be found on top of the Project page on the Flywheel Instance as shown in the snippet below.</div>

<img src="https://gitlab.com/flywheel-io/public/flywheel-tutorials/-/raw/update-old-notebooks-and-TOC/python/assets/find-group-id-and-project-label.png" title="how-to-find-group-id-and-project-label"/>


```python
GROUP_ID = input('Please enter the Group ID that you will use in this notebook: ')
```


```python
PROJECT_LABEL = input('Please enter the Project Labelt that you will use in this notebook: ')
```

`check_user_permission` will return True if both the group and project meet the minimum requirement, else a compatible list will be printed.


```python
check_user_permission(fw, min_reqs, group=GROUP_ID, project=PROJECT_LABEL)
```

# Constants


```python
# Flywheel path to the acquistion container with the file we want to process locally
FW_PATH_TO_ACQ = '<you-group>/<your-project>/<subject.label>/<session.label>/<acquisition.label>'
# Filename of the file we want to process locally
FILENAME = '<the-input-filename-here.ext>'
# Path where the input files will be download
DOWNLOAD_PATH = '/tmp'
```

## Main script

### Find file

Find the flywheel acquisition container by performing a lookup 


```python
acquisition = fw.lookup(FW_PATH_TO_ACQ)
```

Find the file in that acquisition container by name


```python
file = acquisition.get_file(FILENAME)
```

### Download file locally


```python
dest_path = str(Path(DOWNLOAD_PATH) / FILENAME)
```


```python
file.download(dest_path)
```

### Process file locally

This is a very simple processing which is just reorienting the nifti image


```python
reorient = Reorient(orientation='LPS')
reorient.inputs.in_file = dest_path
res = reorient.run()
out_file = res.outputs.out_file
log.info('Output file saved to: %s', out_file)
```

### Create analysis container

Create an analysis container attached to the session with reference to the input files


```python
session = fw.get_session(acquisition.parents.session)
analysis = session.add_analysis(label='My Analysis label', inputs=[file.ref()])
```

### Upload the output file to analysis container


```python
analysis.upload_output(out_file)
```

### Check the uploaded file


```python
analysis = analysis.reload()
```


```python
assert analysis.files[0].name == os.path.basename(out_file)
assert analysis.files[0].size > 0
```
