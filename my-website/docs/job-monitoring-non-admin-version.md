---
id: tutorial_JobMonitoringUserandDeveloperVersion
title: Job Monitoring - User and Developer Version
sidebar_label: Job Monitoring - User and Developer
slug: /tutorial-Job-Monitoring-User-and-Developer
---
**Title**: Job Monitoring - User and Developer Version

**Date**:  July 8th 2020

**Description**:  
A simple tutorial about job monitoring for user and developer only. 

Topics that are included:
1. Jobs that I have launched
2. Filter jobs based on gear name, date range, and state
3. Cancelling Jobs
4. Restarting Jobs
5. Get summary of job status

### **Requirements**:
1. Access to a Flywheel instance.
2. A Flywheel Project with ideally the dataset used in the [upload-data notebook](https://gitlab.com/flywheel-io/public/flywheel-tutorials/-/blob/master/python/upload-data-to-a-new-project.ipynb).
3. Have some jobs running in your Flywheel Project

<div class="alert alert-block alert-warning" >
    <b>NOTE:</b> This notebook is using a test dataset provided by the <a href="https://gitlab.com/flywheel-io/public/flywheel-tutorials/-/blob/master/python/upload-data-to-a-new-project.ipynb" style="color:black">upload-data notebook</a>. If you have not uploaded this test dataset yet, we strongly recommend you do so now following steps in <a href="https://gitlab.com/flywheel-io/public/flywheel-tutorials/-/blob/master/python/upload-data-to-a-new-project.ipynb" style="color:black">here</a> before proceeding because this notebook is based on a specific project structure.
</div>

<div class="alert alert-block alert-danger" >
    <b>WARNING:</b> The metadata of the acquisitions in your test project will be updated and new files will be created after running the scripts below. 
</div>

# Install and Import Dependencies


```python
# Install specific packages required for this notebook
!pip install flywheel-sdk pandas
```


```python
# Import packages
from getpass import getpass
import logging
import os
import datetime
import pprint
from dateutil.tz import tzutc

from IPython.display import display, Image
import flywheel
from permission import check_user_permission

```


```python
# Instantiate a logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
log = logging.getLogger('root')
```

# Flywheel API Key and Client

Get a API_KEY. More on this at in the Flywheel SDK doc [here](https://flywheel-io.gitlab.io/product/backend/sdk/branches/master/python/getting_started.html#api-key).


```python
API_KEY = getpass('Enter API_KEY here: ')
```

Instantiate the Flywheel API client


```python
fw = flywheel.Client(API_KEY if 'API_KEY' in locals() else os.environ.get('FW_KEY'))

del API_KEY
```

Show Flywheel logging information


```python
log.info('You are now logged in as %s to %s', fw.get_current_user()['email'], fw.get_config()['site']['api_url'])
```

***

# Requirements

Before we started our section, we would like to verify that you have the right permission to proceed in this notebook. 



```python
min_reqs = {
"site": "user",
"group": "ro",
"project": ['jobs_view',
            'jobs_run_cancel']
}
```


```python
GROUP_ID = input('Please enter the one of the Group ID that you have access to: ')
```


```python
PORJECT_LABEL = input('Please enter the one Project Label that you have access to: ')
```

`check_user_permission` will return True if both the group and project meet the minimum requirement, else a compatible list will be printed.


```python
check_user_permission(fw, min_reqs)
```

<div class="alert alert-block alert-danger"><b>WARNING:</b> If there is <b>NO Project</b> meet the minimum requirements, you will not be able to proceed in this notebook. Please contact your site admin in order to gain access to run/cancel a job for at least one project on your Flywheel Instance.</div>

***

# How to get Job that I launched?

In this section, we will be showing you how to use `get_current_user_jobs` method to get the jobs that you have launched in the past.


Within the Job container, we will be printing the a few attributes within the job such as the `gear_info` that run the job, `state` of the job, and job `id`. 



```python
jobs = fw.get_current_user_jobs()['jobs']


for i, job in enumerate(jobs):
    print(f'Gear Info: {job.gear_info}')
    print(f'Job State: {job.state}')
    print(f'Job ID: {job.id}')
    print()
    if i > 5:
        break
    
```

> Expected Output:

```
Gear Info: {'category': 'qa',
 'id': None,
 'name': 'mriqc-demo',
 'version': '0.7.0_0.15.1-hbcd-dev-h'}
Job State: complete
Job ID: 5aee8a5e10a8c402961e70f0

Gear Info: {'category': 'qa',
 'id': None,
 'name': 'mriqc-demo',
 'version': '0.7.0_0.15.1-hbcd-dev-h'}
Job State: complete
Job ID: 5bee77bc10a8c402e21e6f1d
```


# Find Specific Job with the Job ID

To view a specific job via the Job ID, you can use `get_job_detail`. This will only work for the job you have launched yourself. 


```python
# Get the latest job that you have launched 
JOB_ID = jobs[0].id
```


```python
specific_job_detail = fw.get_job_detail(JOB_ID)
```


```python
print(f'Gear Info: {specific_job_detail.gear_info}')
print(f'Job State: {specific_job_detail.state}')
print(f'Job ID: {specific_job_detail.id}')
```

> Expected Output:

```
Gear Info: {'category': 'qa', 'id': None, 'name': 'mriqc-demo', 'version': '0.7.0_0.15.1-hbcd-dev-h'}
Job State: complete
Job ID: 5bee77bc10a8c402e21e6f1d
```

***

# Filter Job


In this section, we will showcase how to filter job based on the Gear Name, Date and the State of the Job.

# Initialize a few values

First, we will need you to initialize the gear you would like to filter the job with, date of the jobs that are created by, and state of the job you would like to search by. 


```python
GEAR_NAME = input('Please enter the gear that you would like to filter by: ')
```


```python
CREATED_BY = input('Please enter the date you would like to filter by: ')
```


```python
JOB_STATE = input('Please enter the state of the job you would like to filter by: ')
```

## 1. Gear Name




```python
filtered_job = list(filter(lambda x : x['gear_info']['name'] == GEAR_NAME, jobs))

for i, job in enumerate(filtered_job):
    print(f'Gear Info: {job.gear_info}')
    print(f'Job State: {job.state}')
    print(f'Job ID: {job.id}')
    print()
    if i >= 5:
        break
```

## 2. Date


```python
CREATED_BY = "2020-06-05"
```


```python
def filter_date(job):
    if job.created.strftime("%Y-%m-%d") > CREATED_BY:
        return job
    
```


```python
filtered_job = list(filter(lambda x:x.created.strftime("%Y-%m-%d") > CREATED_BY, jobs))

for i, job in enumerate(filtered_job):
    print(f'Gear Info: {job.gear_info}')
    print(f'Job State: {job.state}')
    print(f'Job ID: {job.id}')
    print()
    if i >= 5:
        break
```

## 3. State of the job


```python
filtered_job = list(filter(lambda x:x.state == JOB_STATE, jobs))

for i, job in enumerate(filtered_job):
    print(f'Gear Info: {job.gear_info}')
    print(f'Job State: {job.state}')
    print(f'Job ID: {job.id}')
    print()
    if i > 5:
        break
```

***

# Cancelling Job

Simply use the `update` method to cancel the job that is on pending.


```python
JOB_STATE = 'pending'

filtered_job = list(filter(JOB_STATE, jobs))

for job in filtered_job:
    job.update(state='cancelled')
    

```

***

# Restarting Job

You can also restart a job that has a state of `failed`. However, each job can only be retried once.

In this example, we will be iterate through the `jobs` list that we defined earlier with `fw.get_current_user_jobs()`. We will only be focusing on retrying the `mriqc` job. Then, we will be using the exception handling to ensure we are retrying job that has not been retried before. A new `job_id` will be generated when it has been successfully retried. This new `job_id` will then be appended to the `retried_job` list.


```python
retried_job = list()

for job in jobs:
    try:
        if job.state == 'failed' and job.gear_info['name'] == 'mriqc' and len(retried_job)< 2:
            new_job_id = fw.retry_job(job.id)
            retried_job.append(new_job_id)
            
    except:
        pass
```

***

# Pulling Statistics of the Jobs

In this section, we want to showcase a simple example on getting a quick summary of the pending and running jobs on your instance. 


```python

pending_jobs = list(filter(lambda x:x.state == 'pending', jobs))

running_jobs = list(filter(lambda x:x.state == 'running', jobs))



```


```python
print(f'==============================\n{datetime.datetime.now()}\n==============================\n')
print(f'Check Job States \n')
print(f'{len(pending_jobs)} pending jobs \n')
print(f'{len(running_jobs)} running jobs \n')
```
